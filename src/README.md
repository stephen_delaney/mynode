# Auxita Code Exercise #

This project has been built using Typescript, Yarn workspaces and Lerna, NestJS, Vue and TypeORM. I 
might have under estimated the time it took to complete, as I wanted to do more work on the views, and 
some refactoring on the weaker parts, but I think the final state of the application captures my knowledge 
on the good and the less good. I have thoroughly enjoyed working on this project.
 
### How do I get set up? ###

* `yarn install` to install module dependencies

*  `yarn build` to build all modules

*  `yarn test` to run tests 

* `yarn start`  to run application on port 3000


Lennart Isaksson
Tue, 2 July 2019




