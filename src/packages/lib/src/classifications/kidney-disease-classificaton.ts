export class KidneyDiseaseClassification {
  public static FAILURE = new KidneyDiseaseClassification(0, 'Failure', {
    less: 15,
  });

  public static SEVERE = new KidneyDiseaseClassification(
    1,
    'Severely Decreased',
    { from: 15, to: 30 }
  );

  public static MODERATE_SEVERE = new KidneyDiseaseClassification(
    2,
    'Moderate to Severe',
    { from: 30, to: 45 }
  );

  public static MILD_MODERATE = new KidneyDiseaseClassification(
    3,
    'Mild to Moderate',
    { from: 45, to: 60 }
  );

  public static MILD = new KidneyDiseaseClassification(4, 'Mildly Decreased', {
    from: 60,
    to: 90,
  });

  public static NORMAL = new KidneyDiseaseClassification(5, 'Normal', {
    from: 90,
  });

  constructor(
    readonly value: number,
    readonly description: string,
    readonly eGFR: any
  ) {}

  static createInstance(type: number): KidneyDiseaseClassification {
    switch (type) {
      case 0:
        return KidneyDiseaseClassification.FAILURE;
      case 1:
        return KidneyDiseaseClassification.SEVERE;
      case 2:
        return KidneyDiseaseClassification.MODERATE_SEVERE;
      case 3:
        return KidneyDiseaseClassification.MILD_MODERATE;
      case 4:
        return KidneyDiseaseClassification.MILD;
      default:
        return KidneyDiseaseClassification.NORMAL;
    }
  }
}
