export class HypertensionClassification {
  public static NO_HYPERTENSION = new HypertensionClassification(
    0,
    'No Hypertension',
    { less: 140 },
    { less: 90 }
  );
  public static STAGE_1 = new HypertensionClassification(
    1,
    'Stage 1',
    { from: 140, to: 160 },
    { from: 90, to: 100 }
  );
  public static STAGE_2 = new HypertensionClassification(
    2,
    'Stage 2',
    { from: 160, to: 180 },
    { from: 100, to: 110 }
  );
  public static STAGE_3 = new HypertensionClassification(
    3,
    'Stage 3',
    { from: 180 },
    { from: 120 }
  );

  constructor(
    readonly value: number,
    readonly description: string,
    readonly SysBP: any,
    readonly DiaBP: any
  ) {}

  static createInstance(type: number): HypertensionClassification {
    switch (type) {
      case 0:
        return HypertensionClassification.NO_HYPERTENSION;
      case 1:
        return HypertensionClassification.STAGE_1;
      case 2:
        return HypertensionClassification.STAGE_2;
      case 3:
        return HypertensionClassification.STAGE_3;
      default:
        return HypertensionClassification.NO_HYPERTENSION;
    }
  }
}
