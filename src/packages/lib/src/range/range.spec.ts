import { RangeBuilder } from './range-builder';
import { RangeFinder } from './range-finder';

describe('Range Tests', () => {
  function rangeFinder(rangeInput): RangeFinder {
    return new RangeFinder(RangeBuilder.build(rangeInput));
  }

  it('should find ranges from and to, and when out of range, finder should return -1', () => {
    const input = [{ from: 0, to: 1 }, { from: 1, to: 2 }, { from: 2, to: 3 }];
    expect(rangeFinder(input).find(-1).range).toEqual(-1);
    expect(rangeFinder(input).find(0).range).toEqual(0);
    expect(rangeFinder(input).find(1).range).toEqual(1);
    expect(rangeFinder(input).find(2).range).toEqual(2);
    expect(rangeFinder(input).find(3).range).toEqual(-1);
  });

  it('should find ranges using less, from and to and from and never return -1', () => {
    const input = [
      { less: 0 },
      { from: 0, to: 1 },
      { from: 1, to: 2 },
      { from: 2 },
    ];
    expect(rangeFinder(input).find(-1000.0).range).toEqual(0);
    expect(rangeFinder(input).find(-1000).range).toEqual(0);
    expect(rangeFinder(input).find(-1).range).toEqual(0);
    expect(rangeFinder(input).find(0).range).toEqual(1);
    expect(rangeFinder(input).find(1).range).toEqual(2);
    expect(rangeFinder(input).find(2).range).toEqual(3);
    expect(rangeFinder(input).find(200).range).toEqual(3);
    expect(rangeFinder(input).find(2000).range).toEqual(3);
    expect(rangeFinder(input).find(2000.0).range).toEqual(3);
  });
});
