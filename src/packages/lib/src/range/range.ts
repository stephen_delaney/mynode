export interface Range {
  matches(position: number): boolean;
}
