import { Range } from './range';

export class RangeResult {
  constructor(
    readonly range: number,
    readonly position: number,
    readonly data?: Range
  ) {}
}
