import { Range } from './range';

export class FromRange implements Range {
  constructor(private readonly from: number) {}

  matches(position: number): boolean {
    return position >= this.from;
  }
}
