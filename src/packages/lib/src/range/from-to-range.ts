import { Range } from './range';

export class FromToRange implements Range {
  constructor(private readonly from: number, private readonly to: number) {}

  matches(position: number): boolean {
    return position >= this.from && position < this.to;
  }
}
