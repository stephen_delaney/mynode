import { RangeResult } from './range-result';
import { Range } from './range';

export class RangeFinder {
  constructor(private readonly range: Range[]) {}

  find(position: number): RangeResult {
    let result: RangeResult = new RangeResult(-1, position);
    this.range.forEach((range: Range, index: number) => {
      if (result.range === -1 && range.matches(position)) {
        result = new RangeResult(index, position, range);
      }
    });
    return result;
  }
}
