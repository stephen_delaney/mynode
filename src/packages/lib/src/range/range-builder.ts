import { FromToRange } from './from-to-range';
import { FromRange } from './from-range';
import { Range } from './range';
import { LessThanRange } from './less-than-range';

export class RangeBuilder {
  static build(
    ranges: Array<{ from?: number; to?: number; less?: number }>
  ): Range[] {
    const result: Range[] = [];
    ranges.forEach((range: { from?: number; to?: number; less?: number }) => {
      if (range.less !== undefined) {
        result.push(new LessThanRange(range.less));
      } else if (range.to === undefined && range.from !== undefined) {
        result.push(new FromRange(range.from));
      } else {
        result.push(new FromToRange(range.from, range.to));
      }
    });
    return result;
  }
}
