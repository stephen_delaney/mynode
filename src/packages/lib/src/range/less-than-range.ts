import { Range } from './range';

export class LessThanRange implements Range {
  constructor(private readonly less: number) {}

  matches(position: number): boolean {
    return position < this.less;
  }
}
