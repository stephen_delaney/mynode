import { HypertensionClassification } from '../classifications';
import { HypertensionCalculator } from './hypertension-calculator';

describe('Hypertension Calculator', () => {
  function getClassification(
    sysBP: number,
    diaBP: number
  ): HypertensionClassification {
    return new HypertensionCalculator(sysBP, diaBP).getClassification();
  }

  describe('SysBP is Greater than or Equal to 180 AND DiaBP Greater than or Equal to 120', () => {
    it('should expect hypertension calculator to resolve only to HyperTensionClassification.STAGE_3', () => {
      expect(getClassification(180, 120)).toEqual(
        HypertensionClassification.STAGE_3
      );
      expect(getClassification(181, 121)).toEqual(
        HypertensionClassification.STAGE_3
      );
      expect(getClassification(200, 200)).toEqual(
        HypertensionClassification.STAGE_3
      );
      expect(getClassification(179, 130)).not.toEqual(
        HypertensionClassification.STAGE_3
      );
      expect(getClassification(190, 119)).not.toEqual(
        HypertensionClassification.STAGE_3
      );
      expect(getClassification(179, 119)).not.toEqual(
        HypertensionClassification.STAGE_3
      );
    });
  });

  describe('If SysBP is between 160 (inclusive) and 180 (exclusive) OR DiaBP is between 100 (inclusive) and 110 (exclusive)', () => {
    it('should expect hypertension calculator to resolve only to HyperTensionClassification.STAGE_2', () => {
      expect(getClassification(160, 100)).toEqual(
        HypertensionClassification.STAGE_2
      );
      expect(getClassification(179, 109)).toEqual(
        HypertensionClassification.STAGE_2
      );
      expect(getClassification(170, 105)).toEqual(
        HypertensionClassification.STAGE_2
      );
      expect(getClassification(159, 109)).not.toEqual(
        HypertensionClassification.STAGE_2
      );
      expect(getClassification(160, 99)).not.toEqual(
        HypertensionClassification.STAGE_2
      );
      expect(getClassification(0, 0)).not.toEqual(
        HypertensionClassification.STAGE_2
      );
      expect(getClassification(300, 300)).not.toEqual(
        HypertensionClassification.STAGE_2
      );
    });
  });

  describe('If SysBP is between 140 (inclusive) and 160 (exclusive) OR DiaBP is between 90 (inclusive) and 100 (exclusive)', () => {
    it('should expect hypertension calculator to resolve only to HyperTensionClassification.STAGE_2', () => {
      expect(getClassification(140, 90)).toEqual(
        HypertensionClassification.STAGE_1
      );
      expect(getClassification(159, 99)).toEqual(
        HypertensionClassification.STAGE_1
      );
      expect(getClassification(160, 90)).not.toEqual(
        HypertensionClassification.STAGE_1
      );
      expect(getClassification(160, 100)).not.toEqual(
        HypertensionClassification.STAGE_1
      );
      expect(getClassification(140, 100)).not.toEqual(
        HypertensionClassification.STAGE_1
      );
    });
  });

  describe('all other cases', () => {
    it('should expect hypertension calculator to resolve only to HyperTensionClassification.NO_HYPERTENSION', () => {
      expect(getClassification(139, 89)).toEqual(
        HypertensionClassification.NO_HYPERTENSION
      );
      expect(getClassification(140, 90)).not.toEqual(
        HypertensionClassification.NO_HYPERTENSION
      );
    });
  });
});
