

export * from './hypertension-calculator';
export * from './hypertension.spec';
export * from './kidney-disease-calculator';
export * from './kidney-disease.spec';
