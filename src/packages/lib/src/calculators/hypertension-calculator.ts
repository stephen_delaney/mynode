import { HypertensionClassification } from '../classifications';
import { RangeBuilder, RangeFinder } from '../range';

export class HypertensionCalculator {
  private sysBPRangeFinder: RangeFinder = new RangeFinder(
    RangeBuilder.build([
      HypertensionClassification.NO_HYPERTENSION.SysBP,
      HypertensionClassification.STAGE_1.SysBP,
      HypertensionClassification.STAGE_2.SysBP,
      HypertensionClassification.STAGE_3.SysBP,
    ])
  );
  private diaBPRangeFinder = new RangeFinder(
    RangeBuilder.build([
      HypertensionClassification.NO_HYPERTENSION.DiaBP,
      HypertensionClassification.STAGE_1.DiaBP,
      HypertensionClassification.STAGE_2.DiaBP,
      HypertensionClassification.STAGE_3.DiaBP,
    ])
  );

  constructor(private readonly sysBP: number, private readonly diaBP: number) {}

  getClassification(): HypertensionClassification {
    const sysBPRange = this.sysBPRangeFinder.find(this.sysBP).range;
    const diaBPRange = this.diaBPRangeFinder.find(this.diaBP).range;
    if (sysBPRange >= 3 && diaBPRange >= 3) {
      return HypertensionClassification.STAGE_3;
    } else if (sysBPRange === 2 && diaBPRange === 2) {
      return HypertensionClassification.STAGE_2;
    } else if (sysBPRange === 1 && diaBPRange === 1) {
      return HypertensionClassification.STAGE_1;
    } else {
      return HypertensionClassification.NO_HYPERTENSION;
    }
  }
}
