import { KidneyDiseaseClassification } from '../classifications';
import { KidneyDiseaseCalculator } from './kidney-disease-calculator';

describe('Kidney Disease Calculator', () => {
  function getClassification(eGFR: number): KidneyDiseaseClassification {
    return new KidneyDiseaseCalculator(eGFR).getClassification();
  }

  describe('eGFR is Greater than or equal to 90 (inclusive)', () => {
    it('should expect kidney disease calculator to resolve only to KidneyDiseaseClassification.NORMAL', () => {
      expect(getClassification(89)).not.toEqual(
        KidneyDiseaseClassification.NORMAL
      );
      expect(getClassification(90)).toEqual(KidneyDiseaseClassification.NORMAL);
      expect(getClassification(100)).toEqual(
        KidneyDiseaseClassification.NORMAL
      );
    });
  });

  describe('eGFR is between 60 and 89 (all inclusive)', () => {
    it('should expect kidney disease calculator to resolve only to KidneyDiseaseClassification.MILD', () => {
      expect(getClassification(60)).toEqual(KidneyDiseaseClassification.MILD);
      expect(getClassification(89)).toEqual(KidneyDiseaseClassification.MILD);
      expect(getClassification(59)).not.toEqual(
        KidneyDiseaseClassification.MILD
      );
      expect(getClassification(90)).not.toEqual(
        KidneyDiseaseClassification.MILD
      );
    });
  });

  describe('eGFR is between 45 and 59 (all inclusive)', () => {
    it('should expect kidney disease calculator to resolve only to KidneyDiseaseClassification.MILD_MODERATE', () => {
      expect(getClassification(45)).toEqual(
        KidneyDiseaseClassification.MILD_MODERATE
      );
      expect(getClassification(59)).toEqual(
        KidneyDiseaseClassification.MILD_MODERATE
      );
      expect(getClassification(44)).not.toEqual(
        KidneyDiseaseClassification.MILD_MODERATE
      );
      expect(getClassification(60)).not.toEqual(
        KidneyDiseaseClassification.MILD_MODERATE
      );
    });
  });

  describe('eGFR is between 30 and 44 (all inclusive)', () => {
    it('should expect kidney disease calculator to resolve only to KidneyDiseaseClassification.MODERATE_SEVERE', () => {
      expect(getClassification(30)).toEqual(
        KidneyDiseaseClassification.MODERATE_SEVERE
      );
      expect(getClassification(44)).toEqual(
        KidneyDiseaseClassification.MODERATE_SEVERE
      );
      expect(getClassification(29)).not.toEqual(
        KidneyDiseaseClassification.MODERATE_SEVERE
      );
      expect(getClassification(45)).not.toEqual(
        KidneyDiseaseClassification.MODERATE_SEVERE
      );
    });
  });

  describe('eGFR is between 15 and 29 (all inclusive)', () => {
    it('should expect kidney disease calculator to resolve only to KidneyDiseaseClassification.SEVERE', () => {
      expect(getClassification(15)).toEqual(KidneyDiseaseClassification.SEVERE);
      expect(getClassification(29)).toEqual(KidneyDiseaseClassification.SEVERE);
      expect(getClassification(14)).not.toEqual(
        KidneyDiseaseClassification.SEVERE
      );
      expect(getClassification(30)).not.toEqual(
        KidneyDiseaseClassification.SEVERE
      );
    });
  });

  describe('everything else', () => {
    it('should expect kidney disease calculator to resolve only to KidneyDiseaseClassification.FAILURE', () => {
      expect(getClassification(14)).toEqual(
        KidneyDiseaseClassification.FAILURE
      );
      expect(getClassification(0)).toEqual(KidneyDiseaseClassification.FAILURE);
      expect(getClassification(15)).not.toEqual(
        KidneyDiseaseClassification.FAILURE
      );
    });
  });
});
