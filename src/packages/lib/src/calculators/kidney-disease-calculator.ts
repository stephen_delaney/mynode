import { RangeBuilder, RangeFinder } from '../range';
import { KidneyDiseaseClassification } from '../classifications';

export class KidneyDiseaseCalculator {
  private eGFRFinder: RangeFinder = new RangeFinder(
    RangeBuilder.build([
      KidneyDiseaseClassification.FAILURE.eGFR,
      KidneyDiseaseClassification.SEVERE.eGFR,
      KidneyDiseaseClassification.MODERATE_SEVERE.eGFR,
      KidneyDiseaseClassification.MILD_MODERATE.eGFR,
      KidneyDiseaseClassification.MILD.eGFR,
      KidneyDiseaseClassification.NORMAL.eGFR,
    ])
  );

  constructor(private readonly eGFR: number) {}

  getClassification(): KidneyDiseaseClassification {
    switch (this.eGFRFinder.find(this.eGFR).range) {
      case 0:
        return KidneyDiseaseClassification.FAILURE;
      case 1:
        return KidneyDiseaseClassification.SEVERE;
      case 2:
        return KidneyDiseaseClassification.MODERATE_SEVERE;
      case 3:
        return KidneyDiseaseClassification.MILD_MODERATE;
      case 4:
        return KidneyDiseaseClassification.MILD;
      default:
        return KidneyDiseaseClassification.NORMAL;
    }
  }
}
