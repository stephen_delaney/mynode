import Vue from 'vue';
import Vuex from 'vuex';
import pathify from './pathify';
import { make } from 'vuex-pathify';
import {
  HypertensionResponseResource,
  KidneyDiseaseResponseResource,
  ValidationErrorResponseResource,
} from '@auxita-code-exercise/api';

Vue.use(Vuex);

export interface StoreData {
  hyperTension: HypertensionResponseResource;
  kidneyDisease: KidneyDiseaseResponseResource;
  validationErrors: ValidationErrorResponseResource;
}

const state: StoreData = {
  hyperTension: new HypertensionResponseResource([], []),
  kidneyDisease: new KidneyDiseaseResponseResource([], []),
  validationErrors: new ValidationErrorResponseResource([]),
};

const mutations = make.mutations(state);
const getters = make.getters(state);

export default new Vuex.Store({
  plugins: [pathify.plugin],
  getters,
  state,
  mutations,
});
