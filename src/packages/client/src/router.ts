import Vue from 'vue';
import Router from 'vue-router';
import HyperTension from '@/views/HyperTension.vue';
import KidneyDisease from '@/views/KidneyDisease.vue';

Vue.use(Router);

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      redirect: '/hypertension',
    },
    {
      path: '/hypertension',
      name: 'hypertension',
      component: HyperTension,
    },
    {
      path: '/kidneydisease',
      name: 'kidneydisease',
      component: KidneyDisease,
    },
  ],
});
