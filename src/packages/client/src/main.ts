import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';
import axios, { AxiosInstance } from 'axios';

Vue.config.productionTip = false;

const instance = axios.create({
  baseURL: 'http://localhost:3000/',
});

const instanceHypertensionApi = axios.create({
  baseURL: 'http://localhost:3000/hypertension/',
});

const instanceKidneyDiseaseApi = axios.create({
  baseURL: 'http://localhost:3000/kidney/',
});

Vue.prototype.$http = instance;
Vue.prototype.$httpHypertensionApi = instanceHypertensionApi;
Vue.prototype.$httpKidneyDiseaseApi = instanceKidneyDiseaseApi;

declare module 'vue/types/vue' {
  export interface Vue {
    $http: AxiosInstance;
    $httpHypertensionApi: AxiosInstance;
    $httpKidneyDiseaseApi: AxiosInstance;
  }
}

new Vue({
  router,
  store,
  render: h => h(App),
}).$mount('#app');
