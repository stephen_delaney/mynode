import CalculatorInput from '@/components/CalculatorInput.vue';
import HypertensionList from '@/components/HypertensionList.vue';
import { HypertensionService } from '@/service/hypertension-service';
import { Mixins } from 'vue-property-decorator';
import Component from 'vue-class-component';
import {
  HypertensionCalculatorRequestResource,
  HypertensionInputSerializer,
  HypertensionJsonInput,
} from '@auxita-code-exercise/api';
import ValidationPrompt from '@/components/ValidationPrompt.vue';

@Component({
  components: {
    CalculatorInput,
    HypertensionList,
    ValidationPrompt,
  },
  mixins: [HypertensionService],
})
export default class HyperTension extends Mixins(HypertensionService) {
  readInput(data: string) {
    this.calculate(this.buildRequest(data));
  }

  buildRequest(data: string): HypertensionCalculatorRequestResource {
    const input: HypertensionJsonInput[] = JSON.parse(HypertensionInputSerializer.toJSON(data));
    return new HypertensionCalculatorRequestResource(input);
  }
}
