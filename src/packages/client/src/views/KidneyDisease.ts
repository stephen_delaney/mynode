import CalculatorInput from '@/components/CalculatorInput.vue';
import { Mixins } from 'vue-property-decorator';
import Component from 'vue-class-component';
import { KidneyDiseaseService } from '@/service/kidney-disease-service';
import {
  KidneyDiseaseCalculatorRequestResource,
  KidneyDiseaseInputDeserializer,
  KidneyDiseaseJsonInput,
} from '@auxita-code-exercise/api';
import KidneyDiseaseList from '@/components/KidneyDiseaseList.vue';
import ValidationPrompt from '@/components/ValidationPrompt.vue';

@Component({
  components: {
    CalculatorInput,
    KidneyDiseaseList,
    ValidationPrompt,
  },
  mixins: [KidneyDiseaseService],
})
export default class KidneyDisease extends Mixins(KidneyDiseaseService) {
  readInput(data: string) {
    this.calculate(this.buildRequest(data));
  }

  buildRequest(data: string): KidneyDiseaseCalculatorRequestResource {
    const input: KidneyDiseaseJsonInput[] = JSON.parse(KidneyDiseaseInputDeserializer.toJSON(data));
    return new KidneyDiseaseCalculatorRequestResource(input);
  }
}
