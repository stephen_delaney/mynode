import Component from 'vue-class-component';
import Vue from 'vue';
import {
  HypertensionCalculatorRequestResource,
  HypertensionResponseResource,
  KidneyDiseaseCalculatorRequestResource,
  KidneyDiseaseResponseResource,
} from '@auxita-code-exercise/api';

@Component({ name: 'httpService' })
export class HttpService extends Vue {
  async calculateKidneyDisease(
    request: KidneyDiseaseCalculatorRequestResource
  ): Promise<KidneyDiseaseResponseResource> {
    return this.$httpKidneyDiseaseApi
      .request({
        url: 'calculate',
        method: 'post',
        headers: {
          'Content-Type': 'application/json',
        },
        data: request,
      })
      .then(res => {
        return Promise.resolve(res.data);
      })
      .catch(res => {
        return Promise.reject(res.response.data);
      });
  }

  async calculateHyperTension(
    request: HypertensionCalculatorRequestResource
  ): Promise<HypertensionResponseResource> {
    return this.$httpHypertensionApi
      .request({
        url: 'calculate',
        method: 'post',
        headers: {
          'Content-Type': 'application/json',
        },
        data: request,
      })
      .then(res => {
        return Promise.resolve(res.data);
      })
      .catch(res => {
        return Promise.reject(res.response.data);
      });
  }
}
