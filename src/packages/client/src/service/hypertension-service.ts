import Vue from 'vue';
import { Component, Watch } from 'vue-property-decorator';
import {
  HypertensionCalculatorRequestResource,
  HypertensionResponseResource,
  ValidationErrorResponseResource,
} from '@auxita-code-exercise/api';
import { HttpService } from '@/service/http-service';

@Component
export class HypertensionService extends Vue {
  async calculate(request: HypertensionCalculatorRequestResource): Promise<any> {
    return await new HttpService()
      .calculateHyperTension(request)
      .then((response: HypertensionResponseResource) => {
        return this.$store.commit('hyperTension', response);
      })
      .catch((errors: ValidationErrorResponseResource) => {
        return this.$store.commit('validationErrors', errors);
      });
  }

  get results(): HypertensionResponseResource[] {
    return (this.$store as any).get('hyperTension');
  }

  @Watch('$route', { immediate: true, deep: true })
  onUrlChange() {
    this.$store.commit('validationErrors', new ValidationErrorResponseResource([]));
  }
}
