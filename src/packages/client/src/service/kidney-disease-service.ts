import Vue from 'vue';
import { Component, Watch } from 'vue-property-decorator';
import { HttpService } from '@/service/http-service';
import {
  KidneyDiseaseCalculatorRequestResource,
  KidneyDiseaseResponseResource,
  ValidationErrorResponseResource,
} from '@auxita-code-exercise/api';

@Component
export class KidneyDiseaseService extends Vue {
  async calculate(request: KidneyDiseaseCalculatorRequestResource): Promise<any> {
    this.$store.commit('validationErrors', new ValidationErrorResponseResource([]));
    return await new HttpService()
      .calculateKidneyDisease(request)
      .then((response: KidneyDiseaseResponseResource) => {
        return this.$store.commit('kidneyDisease', response);
      })
      .catch((errors: ValidationErrorResponseResource) => {
        return this.$store.commit('validationErrors', errors);
      });
  }

  get results(): KidneyDiseaseResponseResource[] {
    return (this.$store as any).get('kidneyDisease');
  }

  @Watch('$route', { immediate: true, deep: true })
  onUrlChange() {
    this.$store.commit('validationErrors', new ValidationErrorResponseResource([]));
  }
}
