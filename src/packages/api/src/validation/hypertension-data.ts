import { IsInt, Validate } from 'class-validator';
import { HypertensionJsonInput } from '../json/hypertension';
import { SimpleDateValidationConstraint } from './simple-date-validation-constraint';

export class HypertensionData implements HypertensionJsonInput {
  @IsInt()
  SysBP: number;

  @IsInt()
  DiaBP: number;

  @Validate(SimpleDateValidationConstraint, {
    message: 'atDate is not a valid date!',
  })
  atDate: string;

  constructor(_SysBP?: number, _DiaBP?: number, _atDate?: string) {
    this.SysBP = _SysBP;
    this.DiaBP = _DiaBP;
    this.atDate = _atDate;
  }
}
