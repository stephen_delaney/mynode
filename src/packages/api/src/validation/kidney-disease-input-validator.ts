import { validate, ValidationError } from 'class-validator';
import { KidneyDiseaseData } from './kidney-disease-data';
import { KidneyDiseaseCalculatorRequestResource } from '../resource/kidney';

export class KidneyDiseaseInputValidator {
  static async validate(
    data: KidneyDiseaseData
  ): Promise<void | ValidationError[]> {
    return await validate(data);
  }

  static async validateRequest(
    request: KidneyDiseaseCalculatorRequestResource
  ): Promise<ValidationError[]> {
    const errors: ValidationError[] = [];
    for (const data of request.data) {
      const error = await KidneyDiseaseInputValidator.validate(
        new KidneyDiseaseData(data.eGFR, data.atDate)
      );
      if (error !== undefined && error instanceof Array && error.length > 0) {
        errors.push(error[0]);
      }
    }
    return errors;
  }
}
