import {
  ValidationArguments,
  ValidatorConstraint,
  ValidatorConstraintInterface,
} from 'class-validator';

@ValidatorConstraint({ name: 'SimpleDateValidationConstraint', async: true })
export class SimpleDateValidationConstraint
  implements ValidatorConstraintInterface {
  validate(text: string, args: ValidationArguments) {
    return !isNaN(Date.parse(text));
  }

  defaultMessage(args: ValidationArguments) {
    return 'Date ($value) is not a valid date!';
  }
}
