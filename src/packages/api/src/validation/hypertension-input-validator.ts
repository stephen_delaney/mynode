import { HypertensionData } from './hypertension-data';
import { validate, ValidationError } from 'class-validator';
import { HypertensionCalculatorRequestResource } from '../resource/hypertension';

export class HypertensionInputValidator {
  static async validate(
    data: HypertensionData
  ): Promise<void | ValidationError[]> {
    return await validate(data);
  }

  static async validateRequest(
    request: HypertensionCalculatorRequestResource
  ): Promise<ValidationError[]> {
    const errors: ValidationError[] = [];
    for (const data of request.data) {
      const error = await HypertensionInputValidator.validate(
        new HypertensionData(data.DiaBP, data.SysBP, data.atDate)
      );
      if (error !== undefined && error instanceof Array && error.length > 0) {
        errors.push(error[0]);
      }
    }
    return errors;
  }
}
