import { IsInt, Validate } from 'class-validator';
import { SimpleDateValidationConstraint } from './simple-date-validation-constraint';
import { KidneyDiseaseJsonInput } from '../json/kidney/kidney-disease-json-input';

export class KidneyDiseaseData implements KidneyDiseaseJsonInput {
  @IsInt()
  eGFR: number;

  @Validate(SimpleDateValidationConstraint, {
    message: 'atDate is not a valid date!',
  })
  atDate: string;

  constructor(_eGFR?: number, _atDate?: string) {
    this.eGFR = _eGFR;
    this.atDate = _atDate;
  }
}
