export class HypertensionResultResource {
  constructor(
    readonly date: string,
    readonly reading: string,
    readonly type: number,
    readonly sysBP: number,
    readonly diaBP: number,
    readonly inputs: any
  ) {}
}
