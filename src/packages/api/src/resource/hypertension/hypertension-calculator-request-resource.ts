import { HypertensionRequestDataResource } from './hypertension-request-data-resource';

export class HypertensionCalculatorRequestResource {
  constructor(readonly data: HypertensionRequestDataResource[]) {}
}
