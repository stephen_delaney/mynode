import { HypertensionResultResource } from './hypertension-result-resource';

export class HypertensionResponseResource {
  constructor(
    readonly entries: HypertensionResultResource[],
    readonly log: HypertensionResultResource[]
  ) {}
}
