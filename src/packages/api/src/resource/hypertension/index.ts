

export * from './hypertension-calculator-request-resource';
export * from './hypertension-request-data-resource';
export * from './hypertension-response-resource';
export * from './hypertension-result-resource';
