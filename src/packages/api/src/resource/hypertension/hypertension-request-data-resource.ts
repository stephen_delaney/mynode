export class HypertensionRequestDataResource {
  constructor(
    readonly SysBP: number,
    readonly DiaBP: number,
    readonly atDate: string
  ) {}
}
