

export * from './kidney-disease-calculator-request-resource';
export * from './kidney-disease-request-resource';
export * from './kidney-disease-response-resource';
export * from './kidney-disease-result-resource';
