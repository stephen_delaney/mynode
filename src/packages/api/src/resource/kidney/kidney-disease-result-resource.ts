export class KidneyDiseaseResultResource {
  constructor(
    readonly date: string,
    readonly reading: string,
    readonly type: number,
    readonly eGFR: number,
    readonly inputs: any,
    public eGFRDrop?: number
  ) {}
}
