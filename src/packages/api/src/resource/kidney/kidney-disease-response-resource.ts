import { KidneyDiseaseResultResource } from './kidney-disease-result-resource';

export class KidneyDiseaseResponseResource {
  constructor(
    readonly entries: KidneyDiseaseResultResource[],
    readonly log: KidneyDiseaseResultResource[]
  ) {}
}
