export class KidneyDiseaseRequestResource {
  constructor(readonly eGFR: number, readonly atDate: string) {}
}
