import { KidneyDiseaseRequestResource } from './kidney-disease-request-resource';

export class KidneyDiseaseCalculatorRequestResource {
  constructor(readonly data: KidneyDiseaseRequestResource[]) {}
}
