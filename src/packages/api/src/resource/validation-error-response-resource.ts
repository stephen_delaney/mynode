export class ValidationErrorResponseResource {
  constructor(readonly constraints: string[]) {}
}
