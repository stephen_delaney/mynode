export interface HypertensionJsonInput {
  SysBP: number;
  DiaBP: number;
  atDate: string;
}
