import { HypertensionInputSerializer } from './hypertension-input-serializer';
import { HypertensionJsonInput } from './hypertension-json-input';
import { HypertensionData, HypertensionInputValidator } from '../../validation';
import { ValidationError } from 'class-validator';
import { HypertensionCalculatorRequestResource } from '../../resource/hypertension';

describe('Hypertension Calculator Validation Input', () => {
  const VALID_DATA =
    '[{SysBP: 120, DiaBP: 90, atDate: ‘2018/10/31’},{ SysBP: 115, DiaBP: 100, atDate: ‘2018/10/20’}]';

  const INVALID_DATE_DATA =
    '[{SysBP: 120, DiaBP: 90, atDate: ‘18/10/31’},{ SysBP: 115, DiaBP: 100, atDate: ‘20-10/20’}]';

  describe('converting and validating user input to json', () => {
    it('should validate and not throw an error', async () => {
      const request: HypertensionCalculatorRequestResource = new HypertensionCalculatorRequestResource(
        JSON.parse(HypertensionInputSerializer.toJSON(VALID_DATA))
      );
      const error = await HypertensionInputValidator.validateRequest(request);
      expect(error.length).toEqual(0);
    });

    it('should not pass on a invalid request', async () => {
      const request: HypertensionCalculatorRequestResource = new HypertensionCalculatorRequestResource(
        JSON.parse(HypertensionInputSerializer.toJSON(INVALID_DATE_DATA))
      );
      const error = await HypertensionInputValidator.validateRequest(request);
      expect(error).toBeInstanceOf(Array);
      expect(error.length).toEqual(2);
      expect(error[0]).toBeInstanceOf(ValidationError);
      expect(error[1]).toBeInstanceOf(ValidationError);
    });

    it('should throw and date exception', async () => {
      const data: HypertensionJsonInput[] = JSON.parse(
        HypertensionInputSerializer.toJSON(INVALID_DATE_DATA)
      );
      for (const record of data) {
        const hypertensionData: HypertensionData = new HypertensionData();
        hypertensionData.SysBP = record.SysBP;
        hypertensionData.DiaBP = record.DiaBP;
        hypertensionData.atDate = record.atDate;
        const error = await HypertensionInputValidator.validate(
          hypertensionData
        );
        expect(error).toBeInstanceOf(Array);
        if (error instanceof Array) {
          expect(error[0]).toBeInstanceOf(ValidationError);
        }
      }
    });
  });
});
