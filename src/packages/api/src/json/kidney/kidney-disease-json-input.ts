export interface KidneyDiseaseJsonInput {
  eGFR: number;
  atDate: string;
}
