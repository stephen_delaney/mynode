import { KidneyDiseaseInputDeserializer } from './kidney-disease-input-deserializer';
import { KidneyDiseaseInputValidator } from '../../validation/kidney-disease-input-validator';
import { ValidationError } from 'class-validator';
import { KidneyDiseaseCalculatorRequestResource } from '../../resource/kidney';

describe('Kidney Failure Calculator Validation Input', () => {
  const VALID_DATA =
    '[{eGFR: 65, atDate: ‘2018/10/31’},{eGFR: 70, atDate: ‘2018/10/20’}]';

  const INVALID_INPUT_DATA =
    '[{eGFR: 65, atDate: ‘18/10/31’},{eGFR: y0, atDate: ‘2018/10/20’}]';

  describe('converting good user input to json', () => {
    it('should validate and not throw an error', async () => {
      const request: KidneyDiseaseCalculatorRequestResource = new KidneyDiseaseCalculatorRequestResource(
        JSON.parse(KidneyDiseaseInputDeserializer.toJSON(VALID_DATA))
      );
      const error = await KidneyDiseaseInputValidator.validateRequest(request);
      expect(error.length).toEqual(0);
    });
  });

  it('should not pass on a invalid request', async () => {
    const request: KidneyDiseaseCalculatorRequestResource = new KidneyDiseaseCalculatorRequestResource(
      JSON.parse(KidneyDiseaseInputDeserializer.toJSON(INVALID_INPUT_DATA))
    );
    const error = await KidneyDiseaseInputValidator.validateRequest(request);
    expect(error).toBeInstanceOf(Array);
    expect(error.length).toEqual(2);
    expect(error[0]).toBeInstanceOf(ValidationError);
    expect(error[1]).toBeInstanceOf(ValidationError);
  });
});
