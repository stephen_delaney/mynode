import { HypertensionInputSerializer } from '../hypertension/hypertension-input-serializer';

export class KidneyDiseaseInputDeserializer {
  static DELIMITER_1: string = '}';
  static DELIMITER_2: string = ',';
  static DELIMITER_3: string = ':';

  static TYPE_NUMBER: number = 1;
  static TYPE_STRING: number = 2;

  static VALID_KEYS = {
    eGFR: { type: HypertensionInputSerializer.TYPE_NUMBER },
    atDate: { type: HypertensionInputSerializer.TYPE_STRING },
  };

  static SANITIZE(str: string) {
    return str.replace(/[^a-zA-Z0-9-.,:\\/}]/g, '');
  }

  static toJSON(input: string): any {
    return JSON.stringify(this.createMapFromInputString(input));
  }

  static createMapFromInputString(input: string) {
    const dataSet: any[] = [];
    const inputTokens: any[] = KidneyDiseaseInputDeserializer.SANITIZE(
      input
    ).split(KidneyDiseaseInputDeserializer.DELIMITER_1);
    inputTokens.forEach((data: string) => {
      const map = {};
      if (data.length && data.length > 0) {
        data
          .split(KidneyDiseaseInputDeserializer.DELIMITER_2)
          .forEach((kvStr: string) => {
            if (kvStr.length && data.length > 0) {
              const keyValue = kvStr.split(
                KidneyDiseaseInputDeserializer.DELIMITER_3
              );
              if (keyValue.length == 2) {
                const key = keyValue[0];
                const val = keyValue[1];
                if (this.VALID_KEYS[key] !== undefined) {
                  const isInt =
                    this.VALID_KEYS[key].type ===
                    HypertensionInputSerializer.TYPE_NUMBER;
                  if (isInt && /^\d+$/.test(val)) {
                    map[keyValue[0]] = parseInt(val);
                  } else {
                    map[keyValue[0]] = val;
                  }
                }
              }
            }
          });
        dataSet.push(map);
      }
    });
    return dataSet;
  }
}
