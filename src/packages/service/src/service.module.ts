import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { HypertensionRepository } from './hypertension/dao/hypertension-repository';
import { HypertensionRepositoryImpl } from './hypertension/dao/hypertension-repository-impl';
import { HypertensionEntity } from './hypertension/entity/hypertension.entity';
import { HypertensionServiceImpl } from './hypertension';
import { KidneyDiseaseEntity } from './kidney/entity';
import { KidneyRepositoryImpl } from './kidney/dao';
import { KidneyServiceImpl } from './kidney';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'sqlite',
      database: ':memory:',
      synchronize: true,
      logging: false,
      entities: [__dirname + '/../**/*.entity{.ts,.js}'],
    }),
    TypeOrmModule.forFeature([HypertensionEntity, KidneyDiseaseEntity]),
  ],
  controllers: [],
  providers: [
    { provide: 'HypertensionRepository', useClass: HypertensionRepositoryImpl },
    { provide: 'HypertensionService', useClass: HypertensionServiceImpl },
    { provide: 'KidneyRepository', useClass: KidneyRepositoryImpl },
    { provide: 'KidneyService', useClass: KidneyServiceImpl },
  ],
})
export class ServiceModule {}
