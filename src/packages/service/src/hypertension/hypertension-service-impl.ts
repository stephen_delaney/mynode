import { HypertensionService } from './hypertension-service';
import { HypertensionServiceRequest } from './request';
import {
  HypertensionLogEntry,
  HypertensionServiceLogResponse,
  HypertensionServiceResponse,
} from './response';
import {
  HypertensionCalculator,
  HypertensionClassification,
  InputDateFormat,
} from '@auxita-code-exercise/lib';
import { HypertensionCalculatorResultReading } from './response/hypertension-calculator-result-reading';
import { HypertensionRepository } from './dao';
import { Inject } from '@nestjs/common';
import { HypertensionEntity } from './entity';
import moment = require('moment');

export class HypertensionServiceImpl implements HypertensionService {
  constructor(
    @Inject('HypertensionRepository')
    readonly repository: HypertensionRepository
  ) {}

  async calculate(
    request: HypertensionServiceRequest
  ): Promise<HypertensionServiceResponse> {
    const hypertensionCalculatorResultReadings: HypertensionCalculatorResultReading[] = [];
    for (const inputData of request.data) {
      const calculator: HypertensionCalculator = new HypertensionCalculator(
        inputData.SysBP,
        inputData.DiaBP
      );
      const classification = calculator.getClassification();
      await this.save(
        inputData.atDate,
        classification,
        inputData.SysBP,
        inputData.DiaBP,
        JSON.stringify(request.data)
      );
      hypertensionCalculatorResultReadings.push(
        new HypertensionCalculatorResultReading(
          inputData.atDate,
          calculator.getClassification(),
          inputData.SysBP,
          inputData.DiaBP,
          inputData
        )
      );
    }
    return Promise.resolve(
      new HypertensionServiceResponse(hypertensionCalculatorResultReadings)
    );
  }

  async save(
    date: string,
    classification: HypertensionClassification,
    sysBP: number,
    diaBP: number,
    entry: string
  ): Promise<HypertensionEntity> {
    let hypertensionEntity = new HypertensionEntity();
    hypertensionEntity.date = moment(date, InputDateFormat.FORMAT).toDate();
    hypertensionEntity.diaBP = diaBP;
    hypertensionEntity.sysBP = sysBP;
    hypertensionEntity.result = classification.description;
    hypertensionEntity.classificationType = classification.value;
    hypertensionEntity.entry = entry;
    return await this.repository.saveEntity(hypertensionEntity);
  }

  async getLog(): Promise<HypertensionServiceLogResponse> {
    const entries: HypertensionLogEntry[] = [];
    const records: HypertensionEntity[] = await this.repository.findAll();
    records.forEach(entity => {
      entries.push(
        new HypertensionLogEntry(
          entity.date,
          HypertensionClassification.createInstance(entity.classificationType),
          entity.sysBP,
          entity.diaBP,
          entity.entry
        )
      );
    });
    return new HypertensionServiceLogResponse(entries);
  }
}
