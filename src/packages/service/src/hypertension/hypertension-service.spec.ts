import { Test } from '@nestjs/testing';
import { ServiceModule } from '../service.module';
import { HypertensionServiceImpl } from './hypertension-service-impl';
import { HypertensionService } from './hypertension-service';
import { HypertensionRepositoryImpl } from './dao/hypertension-repository-impl';
import { HypertensionServiceRequest } from './request';
import { HypertensionServiceResponse } from './response';
import { HypertensionClassification } from '@auxita-code-exercise/lib';
import { TypeOrmModule } from '@nestjs/typeorm';
import { HypertensionEntity } from './entity/hypertension.entity';
import { HypertensionServiceLogResponse } from './response/hypertension-service-log-response';

describe('Hypertension Service', () => {
  let service: HypertensionService;

  beforeAll(async () => {
    const module = await Test.createTestingModule({
      imports: [ServiceModule, TypeOrmModule.forFeature([HypertensionEntity])],
      providers: [
        {
          provide: 'HypertensionRepository',
          useClass: HypertensionRepositoryImpl,
        },
        { provide: 'HypertensionService', useClass: HypertensionServiceImpl },
      ],
    }).compile();
    service = module.get<HypertensionService>('HypertensionService');
  });

  describe('Testing HypertensionService Service', () => {
    it('should not be undefined', async () => {
      expect(service).toBeDefined();
    });
    it('should calculate hypertension and add record to in memory database', async () => {
      let hypertensionServiceRequest = new HypertensionServiceRequest([
        { SysBP: 100, DiaBP: 100, atDate: '2018/10/31' },
      ]);
      let serviceResponse: HypertensionServiceResponse = await service.calculate(
        hypertensionServiceRequest
      );
      expect(serviceResponse.data[0].classification.value).toEqual(
        HypertensionClassification.NO_HYPERTENSION.value
      );
      const response: HypertensionServiceLogResponse = await service.getLog();
      expect(response.logEntries.length).toEqual(1);
      expect(response.logEntries[0].classification).toEqual(
        HypertensionClassification.NO_HYPERTENSION
      );
      expect(response.logEntries[0].sysBP).toEqual(100);
      expect(response.logEntries[0].diaBP).toEqual(100);
    });
  });
});
