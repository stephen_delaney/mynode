import { EntityRepository, Repository } from 'typeorm';
import { HypertensionEntity } from '../entity';
import { InjectRepository } from '@nestjs/typeorm';
import { HypertensionRepository } from './hypertension-repository';

@EntityRepository()
export class HypertensionRepositoryImpl implements HypertensionRepository {
  constructor(
    @InjectRepository(HypertensionEntity)
    private readonly repository: Repository<HypertensionEntity>
  ) {}
  async findAll(): Promise<HypertensionEntity[]> {
    return await this.repository.find({ order: { date: 'DESC' } });
  }
  async saveEntity(entity: HypertensionEntity): Promise<HypertensionEntity> {
    return await this.repository.save(entity);
  }
}
