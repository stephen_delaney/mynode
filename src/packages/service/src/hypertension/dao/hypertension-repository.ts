import { HypertensionEntity } from '../entity';

export interface HypertensionRepository {
  saveEntity(entity: HypertensionEntity): Promise<HypertensionEntity>;
  findAll(): Promise<HypertensionEntity[]>;
}
