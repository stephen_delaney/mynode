export class HypertensionServiceRequest {
  constructor(
    readonly data: { SysBP: number; DiaBP: number; atDate: string }[]
  ) {}
}
