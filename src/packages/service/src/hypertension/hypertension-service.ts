import { HypertensionServiceRequest } from './request';
import {
  HypertensionServiceLogResponse,
  HypertensionServiceResponse,
} from './response';

export interface HypertensionService {
  calculate(
    request: HypertensionServiceRequest
  ): Promise<HypertensionServiceResponse>;
  getLog(): Promise<HypertensionServiceLogResponse>;
}
