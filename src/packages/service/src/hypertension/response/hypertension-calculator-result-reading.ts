import { HypertensionClassification } from '@auxita-code-exercise/lib';

export class HypertensionCalculatorResultReading {
  constructor(
    readonly date: string | Date,
    readonly classification: HypertensionClassification,
    readonly sysBP: number,
    readonly diaBP: number,
    readonly input: any
  ) {}
}
