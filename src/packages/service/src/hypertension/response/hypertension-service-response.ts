import { HypertensionCalculatorResultReading } from './hypertension-calculator-result-reading';

export class HypertensionServiceResponse {
  constructor(readonly data: HypertensionCalculatorResultReading[]) {}
}
