import { HypertensionClassification } from '@auxita-code-exercise/lib';

export class HypertensionLogEntry {
  constructor(
    readonly date: Date,
    readonly classification: HypertensionClassification,
    readonly sysBP: number,
    readonly diaBP: number,
    readonly entry: string
  ) {}
}
