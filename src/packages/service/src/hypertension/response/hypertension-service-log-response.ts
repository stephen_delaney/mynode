import { HypertensionLogEntry } from './hypertension-log-entry';

export class HypertensionServiceLogResponse {
  constructor(readonly logEntries: HypertensionLogEntry[]) {}
}
