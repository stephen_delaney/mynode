import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class HypertensionEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  date: Date;

  @Column()
  sysBP: number;

  @Column()
  diaBP: number;

  @Column()
  result: string;

  @Column()
  classificationType: number;

  @Column()
  entry: string;
}
