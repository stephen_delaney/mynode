export class KidneyDiseaseServiceRequest {
  constructor(readonly data: { eGFR: number; atDate: string }[]) {}
}
