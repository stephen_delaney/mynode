import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class KidneyDiseaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  date: Date;

  @Column()
  eGFR: number;

  @Column()
  result: string;

  @Column()
  classificationType: number;

  @Column()
  entry: string;
}
