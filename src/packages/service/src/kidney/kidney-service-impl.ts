import { Inject } from '@nestjs/common';
import { KidneyRepository } from './dao';
import { KidneyDiseaseServiceRequest } from './request';
import {
  KidneyDiseaseCalculatorResultReading,
  KidneyDiseaseLogEntry,
  KidneyDiseaseServiceLogResponse,
  KidneyDiseaseServiceResponse,
} from './response';
import {
  InputDateFormat,
  KidneyDiseaseCalculator,
  KidneyDiseaseClassification,
} from '@auxita-code-exercise/lib';
import { KidneyDiseaseEntity } from './entity';
import { KidneyService } from './kidney-service';
import moment = require('moment');

export class KidneyServiceImpl implements KidneyService {
  constructor(
    @Inject('KidneyRepository')
    readonly repository: KidneyRepository
  ) {}

  async calculate(
    request: KidneyDiseaseServiceRequest
  ): Promise<KidneyDiseaseServiceResponse> {
    const kidneyFailureCalculatorResultReadings: KidneyDiseaseCalculatorResultReading[] = [];
    for (const inputData of request.data) {
      const calculator: KidneyDiseaseCalculator = new KidneyDiseaseCalculator(
        inputData.eGFR
      );
      const classification = calculator.getClassification();
      await this.save(
        inputData.atDate,
        classification,
        inputData.eGFR,
        JSON.stringify(request.data)
      );
      kidneyFailureCalculatorResultReadings.push(
        new KidneyDiseaseCalculatorResultReading(
          inputData.atDate,
          calculator.getClassification(),
          inputData.eGFR,
          inputData
        )
      );
    }
    return Promise.resolve(
      new KidneyDiseaseServiceResponse(kidneyFailureCalculatorResultReadings)
    );
  }

  async save(
    date: string,
    classification: KidneyDiseaseClassification,
    eGFR: number,
    entry: string
  ): Promise<KidneyDiseaseEntity> {
    let kidneyFailureEntity = new KidneyDiseaseEntity();
    kidneyFailureEntity.date = moment(date, InputDateFormat.FORMAT).toDate();
    kidneyFailureEntity.eGFR = eGFR;
    kidneyFailureEntity.result = classification.description;
    kidneyFailureEntity.classificationType = classification.value;
    kidneyFailureEntity.entry = entry;
    return await this.repository.saveEntity(kidneyFailureEntity);
  }

  async getLog(): Promise<KidneyDiseaseServiceLogResponse> {
    const entries: KidneyDiseaseLogEntry[] = [];
    const records: KidneyDiseaseEntity[] = await this.repository.findAll();
    records.forEach(entity => {
      entries.push(
        new KidneyDiseaseLogEntry(
          entity.date,
          KidneyDiseaseClassification.createInstance(entity.classificationType),
          entity.eGFR,
          entity.entry
        )
      );
    });
    return new KidneyDiseaseServiceLogResponse(entries);
  }
}
