import { EntityRepository, Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { KidneyDiseaseEntity } from '../entity';
import { KidneyRepository } from './kidney-repository';

@EntityRepository()
export class KidneyRepositoryImpl implements KidneyRepository {
  constructor(
    @InjectRepository(KidneyDiseaseEntity)
    private readonly repository: Repository<KidneyDiseaseEntity>
  ) {}
  async findAll(): Promise<KidneyDiseaseEntity[]> {
    return await this.repository.find({ order: { date: 'DESC' } });
  }
  async saveEntity(entity: KidneyDiseaseEntity): Promise<KidneyDiseaseEntity> {
    return await this.repository.save(entity);
  }
}
