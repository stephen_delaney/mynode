import { KidneyDiseaseEntity } from '../entity/kidney-disease.entity';

export interface KidneyRepository {
  saveEntity(entity: KidneyDiseaseEntity): Promise<KidneyDiseaseEntity>;
  findAll(): Promise<KidneyDiseaseEntity[]>;
}
