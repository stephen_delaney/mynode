import { KidneyDiseaseServiceRequest } from './request';
import {
  KidneyDiseaseServiceLogResponse,
  KidneyDiseaseServiceResponse,
} from './response';

export interface KidneyService {
  calculate(
    request: KidneyDiseaseServiceRequest
  ): Promise<KidneyDiseaseServiceResponse>;
  getLog(): Promise<KidneyDiseaseServiceLogResponse>;
}
