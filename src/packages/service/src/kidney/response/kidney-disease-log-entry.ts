import { KidneyDiseaseClassification } from '@auxita-code-exercise/lib';

export class KidneyDiseaseLogEntry {
  constructor(
    readonly date: Date,
    readonly classification: KidneyDiseaseClassification,
    readonly eGFR: number,
    readonly entry: string
  ) {}
}
