import { KidneyDiseaseLogEntry } from './kidney-disease-log-entry';

export class KidneyDiseaseServiceLogResponse {
  constructor(readonly logEntries: KidneyDiseaseLogEntry[]) {}
}
