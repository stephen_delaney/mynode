import { KidneyDiseaseCalculatorResultReading } from './kidney-disease-calculator-result-reading';

export class KidneyDiseaseServiceResponse {
  constructor(readonly data: KidneyDiseaseCalculatorResultReading[]) {}
}
