import { KidneyDiseaseClassification } from '@auxita-code-exercise/lib';

export class KidneyDiseaseCalculatorResultReading {
  constructor(
    readonly date: string | Date,
    readonly classification: KidneyDiseaseClassification,
    readonly eGFR: number,
    readonly input: any
  ) {}
}
