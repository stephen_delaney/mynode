import { Test } from '@nestjs/testing';
import { ServiceModule } from '../service.module';
import { KidneyDiseaseServiceRequest } from './request';
import {
  KidneyDiseaseServiceLogResponse,
  KidneyDiseaseServiceResponse,
} from './response';
import { TypeOrmModule } from '@nestjs/typeorm';
import { KidneyServiceImpl } from './kidney-service-impl';
import { KidneyDiseaseEntity } from './entity';
import { KidneyService } from './kidney-service';
import { KidneyDiseaseClassification } from '@auxita-code-exercise/lib';
import { KidneyRepositoryImpl } from './dao';

describe('Kidney Failure Service', () => {
  let service: KidneyService;

  beforeAll(async () => {
    const module = await Test.createTestingModule({
      imports: [ServiceModule, TypeOrmModule.forFeature([KidneyDiseaseEntity])],
      providers: [
        {
          provide: 'KidneyRepository',
          useClass: KidneyRepositoryImpl,
        },
        { provide: 'KidneyService', useClass: KidneyServiceImpl },
      ],
    }).compile();
    service = module.get<KidneyService>('KidneyService');
  });

  describe('Testing Kidney Failure Service', () => {
    it('should not be undefined', async () => {
      expect(service).toBeDefined();
    });
    it('should calculate kidney failure and add record to in memory database', async () => {
      let kidneyFailureServiceRequest = new KidneyDiseaseServiceRequest([
        { eGFR: 14, atDate: '2018/10/31' },
      ]);
      let serviceResponse: KidneyDiseaseServiceResponse = await service.calculate(
        kidneyFailureServiceRequest
      );
      expect(serviceResponse.data[0].classification.value).toEqual(
        KidneyDiseaseClassification.FAILURE.value
      );
      const response: KidneyDiseaseServiceLogResponse = await service.getLog();
      expect(response.logEntries.length).toEqual(1);
      expect(response.logEntries[0].classification.value).toEqual(
        KidneyDiseaseClassification.FAILURE.value
      );
      expect(response.logEntries[0].eGFR).toEqual(14);
    });
  });
});
