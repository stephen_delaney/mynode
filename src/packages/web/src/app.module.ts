import { Module } from '@nestjs/common';
import {
  HypertensionEntity,
  HypertensionRepositoryImpl,
  HypertensionServiceImpl,
  KidneyDiseaseEntity,
  KidneyRepositoryImpl,
  KidneyServiceImpl,
  ServiceModule,
} from '@auxita-code-exercise/service';
import { HypertensionController } from './hypertension/hypertension.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { KidneyDiseaseController } from './kidney/kidney-disease.controller';

@Module({
  imports: [
    ServiceModule,
    TypeOrmModule.forFeature([HypertensionEntity, KidneyDiseaseEntity]),
  ],
  controllers: [HypertensionController, KidneyDiseaseController],
  providers: [
    { provide: 'HypertensionRepository', useClass: HypertensionRepositoryImpl },
    { provide: 'HypertensionService', useClass: HypertensionServiceImpl },
    { provide: 'KidneyRepository', useClass: KidneyRepositoryImpl },
    { provide: 'KidneyService', useClass: KidneyServiceImpl },
  ],
})
export class AppModule {}
