import {
  HypertensionResponseResource,
  HypertensionResultResource,
} from '@auxita-code-exercise/api';
import {
  HypertensionCalculatorResultReading,
  HypertensionServiceLogResponse,
  HypertensionServiceResponse,
} from '@auxita-code-exercise/service';
import { ResourceBuilder } from '../builder/resource-builder';
import moment = require('moment');
import { InputDateFormat } from '@auxita-code-exercise/lib';

export class HypertensionResourceBuilder extends ResourceBuilder {
  static build(
    response: HypertensionServiceResponse,
    log: HypertensionServiceLogResponse,
  ): HypertensionResponseResource {
    const resource: HypertensionResultResource[] = [];
    const logResource: HypertensionResultResource[] = [];
    response.data.forEach((reading: HypertensionCalculatorResultReading) => {
      resource.push(
        new HypertensionResultResource(
          moment(reading.date).format(InputDateFormat.FORMAT),
          reading.classification.description,
          reading.classification.value,
          reading.sysBP,
          reading.diaBP,
          reading.input,
        ),
      );
    });
    log.logEntries.forEach(logData => {
      logResource.push(
        new HypertensionResultResource(
          moment(logData.date).format(InputDateFormat.FORMAT),
          logData.classification.description,
          logData.classification.value,
          logData.sysBP,
          logData.diaBP,
          '',
        ),
      );
    });
    return new HypertensionResponseResource(resource, logResource);
  }
}
