import {
  Body,
  Controller,
  HttpException,
  HttpStatus,
  Inject,
  Post,
} from '@nestjs/common';
import {
  HypertensionService,
  HypertensionServiceLogResponse,
  HypertensionServiceRequest,
} from '@auxita-code-exercise/service';
import {
  HypertensionCalculatorRequestResource,
  HypertensionInputValidator,
  HypertensionResponseResource,
} from '@auxita-code-exercise/api';
import { HypertensionResourceBuilder } from './hypertension-resource-builder';

@Controller('hypertension')
export class HypertensionController {
  constructor(
    @Inject('HypertensionService')
    private readonly hypertensionService: HypertensionService,
  ) {}

  @Post('calculate')
  async calculate(
    @Body() request: HypertensionCalculatorRequestResource,
  ): Promise<HypertensionResponseResource> {
    const errors = await HypertensionInputValidator.validateRequest(request);
    if (errors.length > 0) {
      throw new HttpException(
        HypertensionResourceBuilder.buildValidationErrorResponse(errors),
        HttpStatus.BAD_REQUEST,
      );
    }
    let calculatorServiceResponse = await this.hypertensionService.calculate(
      new HypertensionServiceRequest(request.data),
    );
    let logResponse: HypertensionServiceLogResponse = await this.hypertensionService.getLog();
    return HypertensionResourceBuilder.build(
      calculatorServiceResponse,
      logResponse,
    );
  }
}
