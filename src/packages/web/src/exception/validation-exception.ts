export class ValidationException {
  constructor(readonly value: string, readonly target: string) {}
}
