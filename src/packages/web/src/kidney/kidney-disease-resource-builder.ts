import {
  KidneyDiseaseCalculatorResultReading,
  KidneyDiseaseServiceLogResponse,
  KidneyDiseaseServiceResponse,
} from '@auxita-code-exercise/service';
import {
  KidneyDiseaseResponseResource,
  KidneyDiseaseResultResource,
} from '@auxita-code-exercise/api';
import { ResourceBuilder } from '../builder/resource-builder';
import moment = require('moment');
import { InputDateFormat } from '@auxita-code-exercise/lib';

export class KidneyDiseaseResourceBuilder extends ResourceBuilder {
  static build(
    response: KidneyDiseaseServiceResponse,
    log: KidneyDiseaseServiceLogResponse,
  ): KidneyDiseaseResponseResource {
    const resource: KidneyDiseaseResultResource[] = [];
    const logResource: KidneyDiseaseResultResource[] = [];
    response.data.forEach((reading: KidneyDiseaseCalculatorResultReading) => {
      resource.push(
        new KidneyDiseaseResultResource(
          moment(reading.date).format(InputDateFormat.FORMAT),
          reading.classification.description,
          reading.classification.value,
          reading.eGFR,
          reading.input,
        ),
      );
    });

    log.logEntries.forEach(logData => {
      logResource.push(
        new KidneyDiseaseResultResource(
          moment(logData.date).format(InputDateFormat.FORMAT),
          logData.classification.description,
          logData.classification.value,
          logData.eGFR,
          '',
          0,
        ),
      );
    });
    KidneyDiseaseResourceBuilder.calculateDrop(logResource);
    return new KidneyDiseaseResponseResource(resource, logResource);
  }

  private static calculateDrop(logResource: KidneyDiseaseResultResource[]) {
    let previous = 0;
    logResource.reverse().forEach((data: KidneyDiseaseResultResource) => {
      data.eGFRDrop = ((previous - data.eGFR) / previous) * 100;
      previous = data.eGFR;
    });
    logResource.reverse();
  }
}
