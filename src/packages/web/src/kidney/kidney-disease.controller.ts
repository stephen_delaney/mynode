import {
  Body,
  Controller,
  HttpException,
  HttpStatus,
  Inject,
  Post,
} from '@nestjs/common';
import {
  KidneyDiseaseServiceLogResponse,
  KidneyDiseaseServiceRequest,
  KidneyService,
} from '@auxita-code-exercise/service';
import {
  KidneyDiseaseCalculatorRequestResource,
  KidneyDiseaseInputValidator,
  KidneyDiseaseResponseResource,
} from '@auxita-code-exercise/api';
import { KidneyDiseaseResourceBuilder } from './kidney-disease-resource-builder';

@Controller('kidney')
export class KidneyDiseaseController {
  constructor(
    @Inject('KidneyService')
    private readonly kidneyService: KidneyService,
  ) {}

  @Post('calculate')
  async calculate(
    @Body() request: KidneyDiseaseCalculatorRequestResource,
  ): Promise<KidneyDiseaseResponseResource> {
    const errors = await KidneyDiseaseInputValidator.validateRequest(request);
    if (errors.length > 0) {
      throw new HttpException(
        KidneyDiseaseResourceBuilder.buildValidationErrorResponse(errors),
        HttpStatus.BAD_REQUEST,
      );
    }
    let kidneyFailureServiceRequest = new KidneyDiseaseServiceRequest(
      request.data,
    );
    let calculatorServiceResponse = await this.kidneyService.calculate(
      kidneyFailureServiceRequest,
    );
    let logResponse: KidneyDiseaseServiceLogResponse = await this.kidneyService.getLog();
    return KidneyDiseaseResourceBuilder.build(
      calculatorServiceResponse,
      logResponse,
    );
  }
}
