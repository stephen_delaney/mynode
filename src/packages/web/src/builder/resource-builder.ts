import { ValidationError } from 'class-validator';
import { ValidationErrorResponseResource } from '@auxita-code-exercise/api';

export class ResourceBuilder {
  static buildValidationErrorResponse(error: ValidationError[]) {
    const messages: string[] = [];
    error.forEach(error => {
      messages.push(JSON.stringify(error.constraints));
    });
    return new ValidationErrorResponseResource(messages);
  }
}
