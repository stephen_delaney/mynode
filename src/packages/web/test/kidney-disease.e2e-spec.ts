import { Test, TestingModule } from '@nestjs/testing';
import * as request from 'supertest';
import { AppModule } from '../src/app.module';
import { KidneyDiseaseResponseResource } from '@auxita-code-exercise/api';
import { KidneyDiseaseClassification } from '@auxita-code-exercise/lib';
import {
  KidneyDiseaseCalculatorRequestResource,
  KidneyDiseaseInputDeserializer,
} from '@auxita-code-exercise/api/dist';

describe('Kidney Failure Controller Test (e2e)', () => {
  let app;

  beforeEach(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();
    app = moduleFixture.createNestApplication();
    await app.init();
  });

  afterEach(async () => {
    await app.close();
  });

  it('/kidney/calculate (POST)', async () => {
    const data = [
      {
        eGFR: KidneyDiseaseClassification.FAILURE.eGFR.less - 1,
        atDate: '2018/10/20',
      },
      {
        eGFR: KidneyDiseaseClassification.SEVERE.eGFR.from,
        atDate: '2018/10/20',
      },
      {
        eGFR: KidneyDiseaseClassification.MODERATE_SEVERE.eGFR.from,
        atDate: '2018/10/20',
      },
      {
        eGFR: KidneyDiseaseClassification.MILD_MODERATE.eGFR.from,
        atDate: '2018/10/20',
      },
      {
        eGFR: KidneyDiseaseClassification.MILD.eGFR.from,
        atDate: '2018/10/20',
      },
      {
        eGFR: KidneyDiseaseClassification.NORMAL.eGFR.from,
        atDate: '2018/10/20',
      },
    ];

    const expectDataValues = [
      [
        KidneyDiseaseClassification.FAILURE.value,
        KidneyDiseaseClassification.FAILURE.description,
      ],
      [
        KidneyDiseaseClassification.SEVERE.value,
        KidneyDiseaseClassification.SEVERE.description,
      ],
      [
        KidneyDiseaseClassification.MODERATE_SEVERE.value,
        KidneyDiseaseClassification.MODERATE_SEVERE.description,
      ],
      [
        KidneyDiseaseClassification.MILD_MODERATE.value,
        KidneyDiseaseClassification.MILD_MODERATE.description,
      ],
      [
        KidneyDiseaseClassification.MILD.value,
        KidneyDiseaseClassification.MILD.description,
      ],
      [
        KidneyDiseaseClassification.NORMAL.value,
        KidneyDiseaseClassification.NORMAL.description,
      ],
    ];

    const response = await request(app.getHttpServer())
      .post('/kidney/calculate')
      .set('Accept', 'application/json')
      .send({ data });
    const responseResource: KidneyDiseaseResponseResource = response.body;

    expect(responseResource.entries.length).toEqual(expectDataValues.length);

    expectDataValues.forEach((expected: any[], index) => {
      expect(responseResource.entries[index].type).toEqual(expected[0]);
      expect(responseResource.entries[index].reading).toEqual(expected[1]);
    });

    return request(response);
  });

  it('/kidney/calculate (POST with input data)', async () => {
    const inputData: string =
      '[{eGFR: 65, atDate: ‘2018/10/31’},{eGFR: 70, atDate: ‘2018/10/20’}]';
    const deSerializedInputData = JSON.parse(
      KidneyDiseaseInputDeserializer.toJSON(inputData),
    );
    const requestResource: KidneyDiseaseCalculatorRequestResource = new KidneyDiseaseCalculatorRequestResource(
      deSerializedInputData,
    );
    const response = await request(app.getHttpServer())
      .post('/kidney/calculate')
      .set('Accept', 'application/json')
      .send(requestResource);
    const responseResource: KidneyDiseaseResponseResource = response.body;
    expect(responseResource.entries.length).toEqual(2);
    return request(response);
  });
});
