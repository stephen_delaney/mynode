import { Test, TestingModule } from '@nestjs/testing';
import * as request from 'supertest';
import { AppModule } from '../src/app.module';
import { HypertensionClassification } from '@auxita-code-exercise/lib';
import {
  HypertensionCalculatorRequestResource,
  HypertensionInputSerializer,
  HypertensionResponseResource,
} from '@auxita-code-exercise/api';

describe('Hypertension Controller Test (e2e)', () => {
  let app;

  beforeEach(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();
    app = moduleFixture.createNestApplication();
    await app.init();
  });

  afterEach(async () => {
    await app.close();
  });

  it('/hypertension/calculate (POST)', async () => {
    const data = [
      {
        SysBP: HypertensionClassification.NO_HYPERTENSION.SysBP.less - 1,
        DiaBP: HypertensionClassification.NO_HYPERTENSION.DiaBP.less - 1,
        atDate: '2018/10/20',
      },
      {
        SysBP: HypertensionClassification.STAGE_1.SysBP.from,
        DiaBP: HypertensionClassification.STAGE_1.DiaBP.from,
        atDate: '2018/10/20',
      },
      {
        SysBP: HypertensionClassification.STAGE_2.SysBP.from,
        DiaBP: HypertensionClassification.STAGE_2.DiaBP.from,
        atDate: '2018/10/20',
      },
      {
        SysBP: HypertensionClassification.STAGE_3.SysBP.from,
        DiaBP: HypertensionClassification.STAGE_3.DiaBP.from,
        atDate: '2018/10/20',
      },
    ];
    const response = await request(app.getHttpServer())
      .post('/hypertension/calculate')
      .set('Accept', 'application/json')
      .send({ data });
    const responseResource: HypertensionResponseResource = response.body;
    expect(responseResource.entries.length).toEqual(4);
    expect(responseResource.entries[0].reading).toEqual(
      HypertensionClassification.NO_HYPERTENSION.description,
    );
    expect(responseResource.entries[0].type).toEqual(
      HypertensionClassification.NO_HYPERTENSION.value,
    );
    expect(responseResource.entries[1].reading).toEqual(
      HypertensionClassification.STAGE_1.description,
    );
    expect(responseResource.entries[1].type).toEqual(
      HypertensionClassification.STAGE_1.value,
    );
    expect(responseResource.entries[2].reading).toEqual(
      HypertensionClassification.STAGE_2.description,
    );
    expect(responseResource.entries[2].type).toEqual(
      HypertensionClassification.STAGE_2.value,
    );
    expect(responseResource.entries[3].reading).toEqual(
      HypertensionClassification.STAGE_3.description,
    );
    expect(responseResource.entries[3].type).toEqual(
      HypertensionClassification.STAGE_3.value,
    );
    return request(response);
  });

  it('/hypertension/calculate (POST with input data)', async () => {
    const inputData: string =
      '[{SysBP: 120, DiaBP: 90, atDate: ‘2018/10/31’},{ SysBP: 115, DiaBP: 100, atDate: ‘2018/10/20’}]';
    const deSerializedInputData = JSON.parse(
      HypertensionInputSerializer.toJSON(inputData),
    );
    const requestResource: HypertensionCalculatorRequestResource = new HypertensionCalculatorRequestResource(
      deSerializedInputData,
    );
    const response = await request(app.getHttpServer())
      .post('/hypertension/calculate')
      .set('Accept', 'application/json')
      .send(requestResource);
    const responseResource: HypertensionResponseResource = response.body;
    expect(responseResource.entries.length).toEqual(2);
    return request(response);
  });
});
